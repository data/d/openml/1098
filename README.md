# OpenML dataset: pubexpendat

https://www.openml.org/d/1098

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Datasets of Data And Story Library, project illustrating use of basic statistic methods, converted to arff format by Hakan Kjellerstrand.
Source: TunedIT: http://tunedit.org/repo/DASL

DASL file http://lib.stat.cmu.edu/DASL/Datafiles/pubexpendat.html

State Public Expenditures
State Spending and Ability to Pay


Reference:  U.S. Department of Commerce, Bureau of the Census, Government Finances
in 1960, Census of Population, 1960,  Census of Manufactures, 1958,  Statistical
Abstract of the United States, 1961.
U.S. Department of Agriculture, Agricultural Statistics, 1961.
U.S. Department of the Interior, Minerals Yearbook, 1960.


Authorization:   free use
Description:  Per capita state and local public expenditures and associated state demographic and
economic characteristics, 1960.

Number of cases:   48

Variable Names:

EX: Per capita state and local public expenditures ($)
ECAB: Economic ability index, in which income, retail sales, and the value of
output (manufactures, mineral, and agricultural) per capita are
equally weighted.
MET: Percentage of population living in standard metropolitan areas
GROW: Percent change in population, 1950-1960
YOUNG: Percent of population aged 5-19 years
OLD: Percent of population over 65 years of age
WEST: Western state (1) or not (0)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1098) of an [OpenML dataset](https://www.openml.org/d/1098). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1098/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1098/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1098/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

